#!/usr/bin/env node

const { execSync } = require('child_process')
const readline = require("readline");

// Create an interface for input and output
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const runCommand = (command) => {
    try {
        execSync(` ${command}`, { stdio: 'inherit' })
    }
    catch (err) {
        console.error(`Failed to execute ${command} `, err)
        return false
    }
    return true
}   

const framework = process.argv[2]

if (!framework) {
    rl.question(
      "For which framework you want to generate airbnb eslint+prettier config ? (vue:1/react:2)",
      (answer) => {
        // Process the answer
        if (answer.toUpperCase() === "1") {
          console.log("You chose Option A.");
        } else if (answer.toUpperCase() === "B") {
          console.log("You chose Option B.");
        } else {
          console.log("Invalid choice. Please enter A or B.");
        }

        // Close the readline interface
        rl.close();
      }
    );
}

